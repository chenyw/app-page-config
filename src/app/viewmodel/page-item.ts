export interface PageItem {
  "pageId": string,
  "pageTitle": string,
  "templateUrl": string,
  "versionId": string,
  "fontSize": number,
  "remoteName": string,
  "moduleName": string,
  "maintainerNo": number,
  "maintainerCode":string,
  "maintainerName":string,
  "systemUser":string,
  "isUse":boolean,
  "entityState":string
}
