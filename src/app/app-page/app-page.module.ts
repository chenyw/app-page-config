import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { PageHeaderComponent } from './page-header/page-header.component';
import { PageBodyComponent } from './page-body/page-body.component';
import { DataService } from './data.service';
import { AppPageRoutingModule } from './app-page-routing.module';
import { FormsModule } from '@angular/forms';

//PrimeNG
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { TabViewModule } from 'primeng/tabview';
import { MultiSelectModule } from 'primeng/multiselect';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';

@NgModule({
  declarations: [
    PageHeaderComponent,
    PageBodyComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    AppPageRoutingModule,
    FormsModule,
    TableModule,
    ButtonModule,
    TabViewModule,
    MultiSelectModule,
    DialogModule,
    InputTextModule,
    DropdownModule
  ],
  exports: [
    PageHeaderComponent
  ],
  providers: [
    DataService
  ]
})
export class AppPageModule { }
