import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { EmpItem } from './../../viewmodel/emp-item';
import { PageItem } from './../../viewmodel/page-item';

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent implements OnInit {
  /*selectedState: any = null;

    states: any[] = [
        {name: 'Arizona', code: 'Arizona'},
        {name: 'California', value: 'California'},
        {name: 'Florida', code: 'Florida'},
        {name: 'Ohio', code: 'Ohio'},
        {name: 'Washington', code: 'Washington'}
    ];*/

    public getEmp: EmpItem[] = [];
    public getData: PageItem[] = [];
    public lastData: PageItem[] = [];
    public newpageId: string = '';
    public versionId: string = '1.0.0';
    public pageTitle: string = '';
    public templateUrl: string = '';
    public viewName: string = '';
    public moduleName: string = '';
    public maintainerName: string = '';
    public newurl: boolean = false;
    public alerturl: boolean = false;

    constructor(public empSvc: DataService) {
      this.empSvc.getDevelopers();
      this.empSvc.getAppPageInfos();
    }

    async ngOnInit() {
      this.getEmp = await this.empSvc.getDevelopers();
      this.getData = await this.empSvc.getAppPageInfos();
      this.lastData = this.getData.slice(-1);
      this.newpageId = this.lastData[0].pageId+1;
      //this.newurl = this.getData.find;
    }

    displayBasic: boolean = false;

    showBasicDialog() {
      this.displayBasic = true;
    }

    cancel(){
      this.displayBasic = false;
      this.newpageId = this.lastData[0].pageId+1;
      this.versionId = '1.0.0';
      this.pageTitle = '';
      this.templateUrl = '';
      this.viewName = '';
      this.moduleName = '';
      this.maintainerName = '';
    }

    checkUrl(value: any){
      /*const newurl = this.getData;
      const result = newurl.find(x => x.templateUrl == value);
      console.log("no");*/
      this.newurl = this.getData.some(x => x.templateUrl == value);
      if(this.newurl! = true){
        this.alerturl = false;
        console.log(this.newurl);
      }
      else{
        this.alerturl = true;
        console.log('no');
      }
    }
}
