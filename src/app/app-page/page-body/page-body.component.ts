import { Component, OnInit } from '@angular/core';
import { PageItem } from './../../viewmodel/page-item';
import { DataService } from '../data.service';

@Component({
  selector: 'app-page-body',
  templateUrl: './page-body.component.html',
  styleUrls: ['./page-body.component.scss']
})

export class PageBodyComponent implements OnInit {

  items: any[];
  selected!: any[];

  constructor(public pageSvc: DataService) {
    this.pageSvc.getAppPageInfos();

    this.items = [
      { name: "頁面代號", inactive: false},
      { name: "頁面標題", inactive: true},
      { name: "網頁路徑", inactive: true},
      { name: "輸出模組名稱", inactive: false},
      { name: "版本", inactive: false},
      { name: "維護人員", inactive: false}
    ];
  }
  ngOnInit() {

  }
  editPage(page:PageItem){
    console.log(page);
  }
  deletePage(page:PageItem){
    console.log(page);
  }

}
