import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo:'/page',
    pathMatch:'full'
  },
  {
      path: 'page',
      loadChildren: () => import('./app-page/app-page.module')
          .then(m => m.AppPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
